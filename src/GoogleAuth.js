import React, { Component } from "react";
import { connect } from "react-redux";
import { signIn, signOut } from "../src/actions/cardActions";
import { withRouter } from 'react-router-dom'
// client Id ev
// 779956184536-5lqrb87foorg4fjfv8an1rqbm97cbcsc.apps.googleusercontent.com

// client Id is
// 779956184536-3ji8r7vqukfe8eso8detfndqnlt65dsm.apps.googleusercontent.com

class GoogleAuth extends Component {
  
  componentDidMount() {
    window.gapi.load("client:auth2", () => {
      window.gapi.client
        .init({
          clientId:
          "779956184536-j4rcj6u7n83uipc7vi0eicvm5kpngfbe.apps.googleusercontent.com",
          scope: "email"
        })
        .then(() => {
          // bu this.auth bir obje yarattı gibi
          this.auth = window.gapi.auth2.getAuthInstance();
          
          this.onAuthChange(this.auth.isSignedIn.get());
          this.auth.isSignedIn.listen(this.onAuthChange);
        });
    });
  }

  onAuthChange = isSignedIn => {
    
    if (isSignedIn) {
      this.props.signIn(this.auth.currentUser.get().getId());
    } else {
      this.props.signOut();
    }
  };

  renderAuthButton() {
    if (this.props.isSignedIn === null) {
      return null;
    } else if (this.props.isSignedIn) {
      return (
        <button onClick={this.onSignOutClick} className="ui red google button">
          <i className="google icon" />
          Sign Out
        </button>
      );
    } else {
      return (
        <button onClick={this.onSignInClick} className="ui red google button">
          <i className="google icon" />
          Sign in with Google
        </button>
      );
    }
  }

  onSignInClick = () => {
    this.auth.signIn().then(this.props.history.push("/homepage"));
  };

  onSignOutClick = () => {
    this.auth.signOut().then(this.props.history.push("/"));
  };

  render() {
    
    return <div>{this.renderAuthButton()}</div>;
  }
}

const mapStateToProps = state => {
  return {
    isSignedIn: state.auth.isSignedIn
  };
};

// mapDispatchToProps yerine kısayol gibi
export default withRouter(connect(
  mapStateToProps,
  { signIn, signOut }
)(GoogleAuth));
