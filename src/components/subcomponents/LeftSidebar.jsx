import React, { Component } from 'react';
import { List, ListItemText, ListItem, Divider } from '@material-ui/core';
import DialogForm from './forms/DialogForm';
import { connect } from 'react-redux';
import { addNewExpense, changeClickedExpense } from '../../actions/cardActions';

const mapStateToProps = (state) => ({
		clickedExpense: state.expense.clickedExpense,
		expenses: state.expense.expenses
});

const mapDispatchToProps = {
	addNewExpense,
	changeClickedExpense
};

class LeftSidebar extends Component {
	
	state = {
		openDialog: false,
	};


	openDialog = () => {
		this.setState({
			...this.state,
			openDialog: !this.state.openDialog
		});
	};

	handleAddNewExpense = (newExpense, userId) => {
		// Buradan action dispatch edecez
		this.props.addNewExpense(newExpense, userId);
	};

	handleCloseDialog = () => {
		this.setState({
			...this.state,
			openDialog: false
		});
	};

	menuItemClickHandler(clickedExpense, index, e) {
		const withIndex = Object.assign(clickedExpense, {index: index})
		
		e.preventDefault();
		this.props.changeClickedExpense(withIndex);
	}

	render() {
		const { expenses, clickedExpense } = this.props;
		
		
		return (
			<List className="leftPanel" component="nav">
				{expenses.length > 0 ?
					expenses.map((expense, index) => (
						<div key={expense.expenseid} >
						<ListItem
							button
							selected={expense.expenseid === clickedExpense.expenseid}
							
							onClick={this.menuItemClickHandler.bind(this, expense, index)}
							name={expense.description}
						>
							<ListItemText primary={expense.description} />
							
						</ListItem>
						<Divider /> </div>
					)) : (<div></div>)}
				<ListItem button>
					<img
						onClick={expenses && expenses.length < 50 ? this.openDialog : null}
						src={require('../../plus.svg')}
					/>
					<ListItemText
						onClick={expenses && expenses.length < 50 ? this.openDialog : null}
						primary={this.props.expenses.length < 50 ? 'Add' : 'Limit asimi'}
					/>
					
				</ListItem>
				{this.state.openDialog === true ? (
					<DialogForm
						handleAddNewExpense={this.handleAddNewExpense}
						open={this.state.openDialog}
						close={this.handleCloseDialog}
					/>
				) : null}
			</List>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(LeftSidebar);
