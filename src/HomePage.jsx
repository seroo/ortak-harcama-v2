import React, { Component } from "react";
import Dashboard from "./components/Dashboard";
// withwidth harika ozellik
import withWidth from "@material-ui/core/withWidth";
import { connect } from "react-redux";
import { loadExpenseFromServer } from "./actions/cardActions";
import { Grid, Paper } from "@material-ui/core/";
import LeftSidebar from "./components/subcomponents/LeftSidebar";
import { withRouter } from "react-router-dom";
import EmptyDashboard from "./components/subcomponents/EmptyDashboard";

var style = {
  // PaperLeft: {
  //   paddingLeft: "8px",
  //   paddingTop: "8px",
  //   paddingRight: "4px"
  // },
  // bodyXs: {
  //   // paddingRight: "8px"
  // },
  // bodySm: {
  //   // paddingRight: "8px",
  //   // marginLeft: "10%",
  //   // marginRight: "10%"
  // },
  // bodyLg: {
  //   // paddingRight: "8px",
  //   // marginLeft: "10%",
  //   // marginRight: "10%"
  // }
};

const mapStateToProps = state => ({
  loaded: state.expense.loaded,
  loading: state.expense.loading,
  error: state.expense.error,
  expenses: state.expense.expenses,
  clickedExpense: state.clickedExpense,
  auth: state.auth,
  showLeftPanel: state.showLeftPanel.showLeftPanel,
});
const mapDispatchToProps = {
  loadExpenseFromServer
};

class HomePage extends Component {
  async componentDidMount() {
    await this.props.loadExpenseFromServer(this.props.auth.userId);
  }

  updateLeftSideBar = () => {
    this.props.loadExpenseFromServer();
  };
  handlewidth = event => {
    if (this.props.width === "xs") {
      this.handleLeftPanelClose();
    }
  };

  render() {

    if (!this.props.auth.isSignedIn) {
      this.props.history.push('/')
    }

    const renderDashboard = this.props.expenses.length > 0 ? <Dashboard/> : <EmptyDashboard/>

    return (
      <div style={this.props.width === "xs" ? style.bodyXs : style.bodySm}>
        <Grid container>
          {this.props.showLeftPanel && (
            <Grid item xs={3} sm={2}>
              <Paper style={{height: "100%"}}>
                <LeftSidebar />
              </Paper>
            </Grid>
          )}
          <Grid
            item
            xs={this.props.showLeftPanel ? 9 : 12}
            sm={this.props.showLeftPanel ? 10 : 12}
          >
            {renderDashboard}
          </Grid>
        </Grid>

      </div>
    );
  }
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(withWidth()(HomePage)));
