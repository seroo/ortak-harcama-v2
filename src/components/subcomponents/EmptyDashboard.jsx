import React from 'react';
import Paper from "@material-ui/core/es/Paper/Paper";

const EmptyDashboard = () => {
    return (
        <Paper style={{height: "100%", paddingTop: "20px"}} >
            <p>{"<= Press button to add an expense"}</p>
        </Paper>
    );
};

export default EmptyDashboard;