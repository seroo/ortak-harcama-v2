import React, { Component } from 'react';
import './App.css';
import { Router, Route } from 'react-router-dom';
// withwidth harika ozellik
import withWidth from '@material-ui/core/withWidth';
import { connect } from 'react-redux';
import { loadExpenseFromServer } from './actions/cardActions';
import Navbar from './components/Navbar';
import Home from './components/Home';
import HomePage from './HomePage';
import createHistory from 'history/createBrowserHistory';

var style = {
	bodyXs: {
		paddingRight: '8px',
	},
	bodySm: {
		paddingRight: '8px',
		marginLeft: '10%',
		marginRight: '10%',
	},
	bodyLg: {
		paddingRight: '8px',
		marginLeft: '10%',
		marginRight: '10%',
	}
};

const mapStateToProps = (state) => ({
	loaded: state.expense.loaded,
	loading: state.expense.loading,
	error: state.expense.error,
	expenses: state.expense.expenses,
	clickedExpense: state.clickedExpense,
	isSignedIn: state.auth.isSignedIn
});
const mapDispatchToProps = {
	loadExpenseFromServer
};
// Stephan ın eğitimindeki gibi ayrı bir dosyaya alayım dedim ama render hatası verdi ben de bu şekilde devam ettim
const history = createHistory();

class App extends Component {
	
	render() {
		return (
			<Router history={history}>
				<div style={this.props.width === 'xs' ? style.bodyXs : style.bodySm}>
					<Navbar />
					<Route path="/" exact component={Home} />
					<Route path="/homepage" exact component={HomePage} />
				</div>
			</Router>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(withWidth()(App));
