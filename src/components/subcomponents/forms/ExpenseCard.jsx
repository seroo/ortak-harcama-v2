import React, { Component } from 'react';
import { TextField, Checkbox, Button, MenuItem, FormControlLabel } from '@material-ui/core';
import DeleteAlertDialog from '../DeleteAlertDialog';
import { deleteExpense, addNewExpense, updateExpense } from '../../../actions/cardActions';
import { connect } from 'react-redux';

const mapStateToProps = (state, ownProps) => {
	return {
	clickedExpense: state.expense.clickedExpense,
	// clickedExpense: ownProps.clickedExpense,
	auth: state.auth,
	
	}
	};

const mapDispatchToProps = {
	deleteExpense,
	updateExpense,
	addNewExpense
};
class ExpenseCard extends Component {
	state = {
		expense: {},
		expenseid: -1,
		description: '',
		place: '',
		variety: '',
		isJoint: 1,
		amount: 0,
		openDialog: false,
		isChangeAThing: false,
	};

	static getDerivedStateFromProps(nextProps, prevState) {
		let update = {};
		if (
			// aynı mı
			!(Object.keys(nextProps.clickedExpense).length === 0 && nextProps.clickedExpense.constructor === Object) &&
			nextProps.clickedExpense !== prevState.expense
		) {
			update.expense = nextProps.clickedExpense;
			update.expenseid = nextProps.clickedExpense.expenseid;
			update.description = nextProps.clickedExpense.description;
			update.place = nextProps.clickedExpense.place;
			update.variety = nextProps.clickedExpense.variety;
			update.isJoint = parseInt(nextProps.clickedExpense.isJoint);
			update.amount = parseFloat(nextProps.clickedExpense.amount);
		}

		return update;
	}

	openDialog = () => {
		this.setState({
			...this.state,
			openDialog: !this.state.openDialog
		});
	};
	handleCloseDialog = () => {
		this.setState({
			...this.state,
			openDialog: false
		});
	};

	handleChange = (name) => (event) => {
		
		this.setState({
			...this.state,
			[name]: event.target.value,
			isChangeAThing: true
		});
	};

	handleCheckedChange = (name) => (event) => {
		this.setState({
			...this.state,
			isJoint: event.target.checked ? 1 : 0,
			isChangeAThing: true
		});
	};
	handleDelete = () => {
		this.props.deleteExpense(this.state.expense.expenseid, this.props.auth.userId);
		this.setState({
			...this.state,
			expense: null
		});
	};
	handleUpdate = () => {
		if (this.state.expense === {}) {
			this.props.addNewExpense(this.state);
		} else {
			
			this.props.updateExpense(this.state, this.props.auth.userId, this.props.clickedExpense);
			this.setState({
				...this.state,
				isChangeAThing: false,
			})
		}
	};
	handleSelect = (event) => {
		this.setState({
			...this.state,
			variety: event.target.value,
			isChangeAThing: true,
		});
	};

	render() {
		if (this.state.amount === "") {
			this.state.isChangeAThing = false;
		}
		
		return (
			<div style={{ paddingBottom: "12px", marginLeft: "16px" }}>
				<form>
					<TextField
						style={{ margin: '10px 10px' }}
						label="Harcama adi"
						placeholder="Yemek, içmek vb.."
						onChange={this.handleChange('description')}
						value={this.state.description}
					/>
					<TextField
						style={{ margin: '10px 10px' }}
						label="Yer/Mekan"
						onChange={this.handleChange('place')}
						value={this.state.place}
						placeholder="Nusret/Chia vb.."
					/>
					<TextField
						style={{ margin: '10px 10px' }}
						label="Variety"
						select
						onChange={this.handleSelect}
						value={this.state.variety}
					>
						<MenuItem name={'yemek'} value={'yemek'}>
							Yemek
						</MenuItem>
						<MenuItem name={'icmek'} value={'icmek'}>
							Icmek
						</MenuItem>
						<MenuItem name={'etkinlik'} value={'etkinlik'}>
							Etkinlik
						</MenuItem>
						<MenuItem name={'diger'} value={'diger'}>
							Diger
						</MenuItem>
					</TextField>
					{/*<FormControlLabel*/}
						{/*control={*/}
							{/*<Checkbox*/}
								{/*checked={this.state.isJoint === 1}*/}
								{/*onChange={this.handleCheckedChange('isJoint')}*/}
								{/*color="primary"*/}
							{/*/>*/}
						{/*}*/}
						{/*label="Herkes ortak mı?"*/}
					{/*/>*/}
					<TextField
						style={{ margin: '10px 10px' }}
						label="Harcama Tutarı"
						error
						type="number"
						value={this.state.amount}
						onChange={this.handleChange('amount')}
						placeholder="0.00"
					/>
				</form>
				<div style={{ marginTop: '12px', marginLeft: '10px' }}>
					<Button
						disabled={this.state.isChangeAThing === false}
						variant="contained"
						color="primary"
						onClick={this.handleUpdate}
					>
						Submit
					</Button>
					<Button
						style={{ marginLeft: '16px' }}
						onClick={this.openDialog}
						variant="contained"
						color="secondary"
					>
						Delete
					</Button>
					{this.state.openDialog === true ? (
						<DeleteAlertDialog
							open={this.state.openDialog}
							close={this.handleCloseDialog}
							handleDelete={this.handleDelete}
						/>
					) : null}
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ExpenseCard);
