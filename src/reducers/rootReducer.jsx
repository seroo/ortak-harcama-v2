import { combineReducers } from 'redux';
import participantsReducer from './participantsReducer'
import expensesReducer from './expenseReducer'
import authReducer from './authReducer';
import leftPanelReducer from './leftPanelReducer'

const rootReducer = combineReducers({
    participant: participantsReducer,
    expense: expensesReducer,
    auth: authReducer,
    showLeftPanel: leftPanelReducer,
})

export default rootReducer;