import React, { Component } from 'react';
import {
	AppBar,
	Toolbar,
	Typography,
	Drawer,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
	Divider,
	IconButton
} from '@material-ui/core/';
import MailIcon from '@material-ui/icons/Mail';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import {toggleLeftPanel} from '../actions/cardActions';

import GoogleAuth from '../GoogleAuth';
import { connect } from 'react-redux';
import MenuIcon from '@material-ui/icons/Menu';

// const styles = {
// 	root: {
// 		flexGrow: 1
// 	},
// 	grow: {
// 		flexGrow: 1
// 	},
// 	menuButton: {
// 		marginLeft: -12,
// 		marginRight: 20
// 	}
// };
class Navbar extends Component {
	state = {
		// Navigation Drawer var ama şimdilik iptal ettim buttonu gizledim
		left: false,
	};
	toggleDrawer = (side, open) => () => {

		this.setState({
			...this.state,
			[side]: open
		});
	};
	toggleSidebarHandler = (e) => {
		e.preventDefault();
		
		this.props.toggleLeftPanel();
	};
	
	render() {
		
		const sideList = (
			<div>
				<List>
					{[ 'Inbox', 'Starred', 'Send email', 'Drafts' ].map((text, index) => (
						<ListItem button key={text}>
							<ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
							<ListItemText primary={text} />
						</ListItem>
					))}
				</List>
				<Divider />
				<List>
					{[ 'All mail', 'Trash', 'Spam' ].map((text, index) => (
						<ListItem button key={text}>
							<ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
							<ListItemText primary={text} />
						</ListItem>
					))}
				</List>
			</div>
		);
		return (
			<AppBar className="appbar" position="static">
				<Toolbar  style={{ marginTop: '10px'}}>
					{/* <IconButton onClick={this.toggleDrawer('left', true)} color="inherit" aria-label="Menu">
						<MenuIcon />
					</IconButton> */}
					<Drawer open={this.state.left} onClose={this.toggleDrawer('left', false)}>
						<div
							tabIndex={0}
							role="button"
							onClick={this.toggleDrawer('left', false)}
							onKeyDown={this.toggleDrawer('left', false)}
						>
							{sideList}
						</div>
					</Drawer>
					<IconButton onClick={this.toggleSidebarHandler} color="inherit"
              aria-label="Open drawer">
			  		<MenuIcon />
					</IconButton>
					<Typography variant="h6" color="inherit">
						ORTAK HARCAMA
					</Typography>
					<div className="right-menu" style={{marginLeft:"auto", marginRight:"5px" }} ><GoogleAuth  /></div>
					
				</Toolbar>
			</AppBar>
		);
	}
}

const mapStateToProps = (state) => {
	
	return {
	showLeftPanel: state.showLeftPanel.showLeftPanel,
	}
}
const mapDispatchToProps = {
	toggleLeftPanel,
}

export default connect(mapStateToProps, mapDispatchToProps) (Navbar);
