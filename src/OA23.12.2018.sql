-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1:3306
-- Üretim Zamanı: 22 Ara 2018, 23:57:36
-- Sunucu sürümü: 10.2.17-MariaDB
-- PHP Sürümü: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `u692822339_denem`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `expenses`
--

CREATE TABLE `expenses` (
  `expenseid` int(11) NOT NULL,
  `description` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `variety` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `place` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `isJoint` tinyint(4) NOT NULL DEFAULT 1,
  `amount` float NOT NULL DEFAULT 0,
  `rezerv` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tablo döküm verisi `expenses`
--

INSERT INTO `expenses` (`expenseid`, `description`, `variety`, `place`, `isJoint`, `amount`, `rezerv`, `date`) VALUES
(55, 'werfd11212', 'yemek', 'fddsSs', 1, 400, '101270801987535879208', '2018-12-21 14:19:25'),
(57, '3343', 'yemek', '', 1, 323, '101270801987535879208', '2018-12-21 14:19:40'),
(59, '321', 'yemek', '', 1, 123, '101270801987535879208', '2018-12-22 00:22:14');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `participants`
--

CREATE TABLE `participants` (
  `participantid` int(11) NOT NULL,
  `name` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `isJoint` tinyint(4) DEFAULT 1,
  `amountOfJoint` float NOT NULL DEFAULT 1,
  `amountOfCut` float NOT NULL DEFAULT 1,
  `isPaid` tinyint(4) NOT NULL DEFAULT 0,
  `expenseid` int(11) NOT NULL DEFAULT -1,
  `rezerv` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tablo döküm verisi `participants`
--

INSERT INTO `participants` (`participantid`, `name`, `description`, `isJoint`, `amountOfJoint`, `amountOfCut`, `isPaid`, `expenseid`, `rezerv`, `date`) VALUES
(44, 'sd2', '', 0, 2, 0, 0, 57, 0, 2147483647),
(46, 'ee', '', 0, 31, 1.5, 0, 55, 0, 2147483647),
(47, 'ee2', '', 1, 123, 0, 0, 55, 0, 2147483647),
(48, '123123', '', 1, 0, 0, 0, 55, 0, 2147483647);

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`expenseid`);

--
-- Tablo için indeksler `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`participantid`),
  ADD KEY `participants_ibfk_1` (`expenseid`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `expenses`
--
ALTER TABLE `expenses`
  MODIFY `expenseid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- Tablo için AUTO_INCREMENT değeri `participants`
--
ALTER TABLE `participants`
  MODIFY `participantid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- Dökümü yapılmış tablolar için kısıtlamalar
--

--
-- Tablo kısıtlamaları `participants`
--
ALTER TABLE `participants`
  ADD CONSTRAINT `participants_ibfk_1` FOREIGN KEY (`expenseid`) REFERENCES `expenses` (`expenseid`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
