-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1:3306
-- Üretim Zamanı: 21 Ara 2018, 14:06:51
-- Sunucu sürümü: 10.2.17-MariaDB
-- PHP Sürümü: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `u692822339_denem`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `expenses`
--

CREATE TABLE `expenses` (
  `expenseid` int(11) NOT NULL,
  `description` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `variety` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `place` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `isJoint` tinyint(4) NOT NULL DEFAULT 1,
  `amount` int(11) NOT NULL DEFAULT 0,
  `rezerv` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tablo döküm verisi `expenses`
--

INSERT INTO `expenses` (`expenseid`, `description`, `variety`, `place`, `isJoint`, `amount`, `rezerv`, `date`) VALUES
(47, 'içmeks', 'icmek', 'chia', 0, 22, '1', '2018-12-20 20:50:52'),
(51, 'yemek', 'yemek', 'sanayi', 1, 17, '101270801987535879208', '2018-12-21 14:03:56'),
(52, 'icmek', 'icmek', 'beşiktaş', 1, 320, '101270801987535879208', '2018-12-21 14:04:56'),
(53, 'konser', 'etkinlik', 'dorock', 1, 324, '101270801987535879208', '2018-12-21 14:06:17');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `participants`
--

CREATE TABLE `participants` (
  `participantid` int(11) NOT NULL,
  `name` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `isJoint` tinyint(4) DEFAULT 1,
  `amountOfJoint` int(11) NOT NULL DEFAULT 1,
  `amountOfCut` int(11) NOT NULL DEFAULT 1,
  `isPaid` tinyint(4) NOT NULL DEFAULT 0,
  `expenseid` int(11) NOT NULL DEFAULT -1,
  `rezerv` int(11) NOT NULL,
  `date` int(11) NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Tablo döküm verisi `participants`
--

INSERT INTO `participants` (`participantid`, `name`, `description`, `isJoint`, `amountOfJoint`, `amountOfCut`, `isPaid`, `expenseid`, `rezerv`, `date`) VALUES
(34, 'sdfsd', 'ds', 0, 222, 22, 0, 47, 0, 2147483647),
(39, 'harun', '', 0, 1, 0, 0, 51, 0, 2147483647),
(40, 'apo', '', 0, 1, 0, 0, 51, 0, 2147483647),
(41, 'atıl', '', 0, 3, 0, 0, 52, 0, 2147483647),
(42, 'sevgin', '', 0, 33, 0, 0, 53, 0, 2147483647);

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`expenseid`);

--
-- Tablo için indeksler `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`participantid`),
  ADD KEY `participants_ibfk_1` (`expenseid`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `expenses`
--
ALTER TABLE `expenses`
  MODIFY `expenseid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- Tablo için AUTO_INCREMENT değeri `participants`
--
ALTER TABLE `participants`
  MODIFY `participantid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- Dökümü yapılmış tablolar için kısıtlamalar
--

--
-- Tablo kısıtlamaları `participants`
--
ALTER TABLE `participants`
  ADD CONSTRAINT `participants_ibfk_1` FOREIGN KEY (`expenseid`) REFERENCES `expenses` (`expenseid`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
