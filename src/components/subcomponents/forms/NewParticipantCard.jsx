import React, { Component } from 'react';
import { TextField, FormControlLabel, Checkbox, Button } from '@material-ui/core';


class NewParticipantCard extends Component {
	state = {
		expenseid: -1,
		name: '',
		description: '',
		isJoint: 1,
		isPaid: 0,
		amountOfJoint: '',
		amountOfCut: '',
		openDialog: false,
		isChangeAThing: false,
	};

	static getDerivedStateFromProps(nextProps, prevState) {
		let update = {};
		if (nextProps.clickedExpense !== []) {
			update.expenseid = nextProps.clickedExpense.expenseid;
		}
		return update;
	}


	handleChange = (name) => (event) => {
		this.setState({
			...this.state,
			[name]: event.target.value,
			isChangeAThing: true,
		});
	};

	// TODO SONRA HALLET
	clearForm = () => {
		this.setState({
			...this.state,
			name: '',
			description: '',
			isJoint: 0,
			isPaid: 0,
			amountOfJoint: '',
			amountOfCut: '',
			isChangeAThing: false,
		});
	};

	handleCheckedChange = (name) => (event) => {
		this.setState({
			...this.state,
			[name]: event.target.checked ? 1 : 0,
			isChangeAThing: true,
		});
	};

	handleSubmit = () => {
		this.props.dialogFormSubmitted(this.state);
		this.setState({...this.state, isChangeAThing: false})
		this.props.onClose();
	};

	handleCloseDialog = () => {
		this.setState({
			...this.state,
			openDialog: !this.state.open
		});
	};

	render() {
		if (this.state.name === "") {
			this.state.isChangeAThing = false;
		}

		return (
			<div>
				<form>
					<TextField
						style={{ margin: '10px 10px' }}
						label="Name"
						placeholder="Katilimci Adi"
						onChange={this.handleChange('name')}
						value={this.state.name}
					/>
					<TextField
						style={{ margin: '10px 10px' }}
						label="Aciklama"
						multiline
						onChange={this.handleChange('description')}
						value={this.state.description}
						placeholder="Aciklama.."
					/>
					<FormControlLabel
						control={
							<Checkbox
								checked={this.state.isJoint === 1}
								onChange={this.handleCheckedChange('isJoint')}
								color="primary"
							/>
						}
						label="Harcamaya katılıyor.."
					/>
					<FormControlLabel
						control={
							<Checkbox
								checked={this.state.isPaid === 1}
								onChange={this.handleCheckedChange('isPaid')}
								color="primary"
							/>
						}
						label="Ödeme alındı mı.."
					/>
					<TextField
						style={{ margin: '10px 10px' }}
						label="Uygulanacak indirim"
						type="number"
						value={this.state.amountOfCut}
						onChange={this.handleChange('amountOfCut')}
						placeholder="0.00"
					/>
				</form>
				<div style={{ marginTop: '32px', marginLeft: '10px' }}>
					<Button
						disabled={this.state.isChangeAThing === false}
						variant="contained"
						color="primary"
						onClick={this.handleSubmit}
					>
						Submit
					</Button>
					<Button
						style={{ marginLeft: '16px' }}
						onClick={this.clearForm}
						variant="contained"
						color="secondary"
					>
						Clear
					</Button>
				</div>
			</div>
		);
	}
}

export default NewParticipantCard;
