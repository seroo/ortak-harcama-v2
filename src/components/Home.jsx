import React from 'react'
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import GoogleAuth from "../GoogleAuth"
import { Typography } from '@material-ui/core';

class Home extends React.Component {
 // class yap auth a göre direk Homepage e yönlendir.
// sign out yapınca Redirectte  
style = {
  container: {
    padding: "10px",
    
    textAlign: "center",
  }
}

render() {
    if (this.props.auth.isSignedIn) {
            this.props.history.push("/homepage")
    }  
    /**
      Typography KULLANIRKEN HATA ALMAMAK İCİN INDEX.HTML YE EKLE
      <script>
      window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true
    </script>
     */
return (
    <div style={this.style.container} >
    
      <Typography style={{marginTop: "16px"}} variant="h5" >LÜTFEN GİRİŞ YAPINIZ</Typography>
      <div className="right-menu" style={{marginLeft:"auto", marginRight:"5px" }} ><GoogleAuth  /></div>
      <Typography style={{marginTop: "16px"}} variant="caption" >{"(*) Google login " +
       "ile şifreniz hiçbir şekilde site bünyesinde barındırılmamaktadır. Detaylı bilgi"}<a href='https://support.google.com/accounts/answer/112802?co=GENIE.Platform%3DDesktop&hl=tr' target="_blank" rel="noopener noreferrer" > için...</a> </Typography>
      

    </div>
    
  )
}
}
const mapStateToProps = (state) => {
    return {
      auth: state.auth
    }
}

export default withRouter(connect(mapStateToProps, null) (Home));
