import { TOGGLE_LEFT_PANEL } from '../actions/actionConstants'

const initialState = {
    showLeftPanel: true,
}

const leftPanelReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_LEFT_PANEL :
            return {...state, showLeftPanel: !(state.showLeftPanel)}
        
        default:
            return state;
    }
}

export default leftPanelReducer