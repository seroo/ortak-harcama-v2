import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

class DeleteAlertDialog extends Component {
	state = {
		open: true
	};

	handleClose = () => {
		this.setState({ open: false });
		this.props.close();
	};
	handleDelete = () => {
		this.props.handleDelete();
		this.handleClose();
	}
	componentDidMount() {
		if (this.props.open) {
			this.setState({ ...this.state, open: this.props.open });
		}
	}

	render() {
		return (
			<Dialog
				open={this.state.open}
				onClose={this.handleClose}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
			>
				<DialogTitle id="alert-dialog-title">{"Silmek istediğinize emin misiniz?"}</DialogTitle>
				<DialogContent>
					<DialogContentText id="alert-dialog-description">
						
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={this.handleClose} color="primary">
						Abort
					</Button>
					<Button onClick={this.handleDelete} color="primary" autoFocus>
						Delete
					</Button>
				</DialogActions>
			</Dialog>
		);
	}
}

export default DeleteAlertDialog;
