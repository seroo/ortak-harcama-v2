

export const allJoint = (participants) => {
    
    const joints = [];
    for (let i = 0; i < participants.length; i++) {
        joints.push(parseInt(participants[i].isJoint));
    }
    for (let i = 0; i < joints.length; i++) {
        if (joints[i] !== 1) {
            return false;
        }
    }
    return true;
} 