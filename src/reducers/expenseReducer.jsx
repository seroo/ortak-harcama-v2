import {
  SAVE_EXPENSE_START,
  SAVE_EXPENSE_DONE,
  SAVE_EXPENSE_ERROR,
  LOAD_EXPENSES_FROM_SERVER,
  LOAD_EXPENSES_START,
  LOAD_EXPENSES_DONE,
  LOAD_EXPENSES_ERROR,
  DELETE_EXPENSE_DONE,
  DELETE_EXPENSE_ERROR,
  DELETE_EXPENSE_START,
  UPDATE_EXPENSE_DONE,
  UPDATE_EXPENSE_ERROR,
  UPDATE_EXPENSE_START,
  CHANGE_CLICKED_EXPENSE_START,
  CHANGE_CLICKED_EXPENSE_DONE,
  CHANGE_CLICKED_EXPENSE_ERROR,
} from "../actions/actionConstants";

const initialState = {
  loaded: false,
  loading: false,
  error: null,
  expenses: [],
  saving: false,
  deleting: false,
  updating: false,
  clickedExpense: {},
};
// expenses[action.payload.clickedExpense.index]: clickedExpense,
const expensesReducer = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_EXPENSE_START:
      return {...state, saving: true};
    case SAVE_EXPENSE_DONE:
      return {...state, saving: false};
    case SAVE_EXPENSE_ERROR:
      return {...state, error: action.payload};
    case UPDATE_EXPENSE_START:
      return {...state, updating: true};
    case UPDATE_EXPENSE_DONE:
    const index = state.expenses.map(obj => obj.expenseid).indexOf(action.payload.updatedExpense.expenseid)
    
    state.expenses[index] = action.payload.updatedExpense
    state.clickedExpense = state.expenses[index]

      return {
        ...state, 
        updating: false,
        expenses: state.expenses
       //clickedExpense: action.payload.clickedExpense
      };
    case UPDATE_EXPENSE_ERROR:
      return {...state, error: action.payload};
    case CHANGE_CLICKED_EXPENSE_START:
      return state;
    case CHANGE_CLICKED_EXPENSE_DONE:

      return {...state, clickedExpense: action.payload };
    case CHANGE_CLICKED_EXPENSE_ERROR:
      return {...state, error: action.payload};
    case DELETE_EXPENSE_START:
      return {...state, deleting: true};
    case DELETE_EXPENSE_DONE:
      return {...state, deleting: false};
    case DELETE_EXPENSE_ERROR:
      return {...state, error: action.payload};
    case LOAD_EXPENSES_START:
      return {
        ...state,
        loaded: false,
        loading: true,
        error: null
      };
    case LOAD_EXPENSES_DONE:
      return {
        ...state,
        loaded: true,
        loading: false,
		error: null,
		expenses: action.payload
      };
    case LOAD_EXPENSES_ERROR:
      return {
        ...state,
        loaded: false,
        loading: false,
        error: action.payload
      };
    case LOAD_EXPENSES_FROM_SERVER:
      return {
		...state,
        loading: true,
        error: null
      };
    default:
      return state;
  }
};

export default expensesReducer;
