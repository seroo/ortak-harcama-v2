import React, { Component } from 'react';
import ExpenseCard from './subcomponents/forms/ExpenseCard';
import { Grid, Paper, Fab } from '@material-ui/core/';
import AddIcon from '@material-ui/icons/Add';
import ParticipantCard from './subcomponents/forms/ParticipantCard';
import { connect } from 'react-redux';
import DialogNewParticipant from '../components/subcomponents/forms/DialogNewParticipant';
import { loadParticipantsOfExpense } from './../actions/cardActions';



// INSERT INTO tbl (value1, value2) VALUES (A, B)
// SELECT SCOPE_IDENTITY()   -- post ederken id yi geri dönuyormus

// var style = {
// };

class Dashboard extends Component {
	state = {
		// participants: [],
		loaded: false,
		// clickedExpense: {},
		openDialog: false,
		removeParticipants: false,
	};
	openDialog = () => {
		this.setState({
			...this.state,
			openDialog: !this.state.openDialog
		});
	};
	handleCloseDialog = () => {
		this.setState({
			...this.state,
			openDialog: false
		});
	};
	static getDerivedStateFromProps(nextProps, prevState) {
		
		let update = {};
		// if (nextProps.clickedExpense !== []) {
		// 	update.clickedExpense = nextProps.clickedExpense;
		// // }
		// if (nextProps.loaded !== []) {
		// 	update.loaded = nextProps.loaded;
		// }
		
		if (nextProps.participants !== undefined && nextProps.participants !== [] && nextProps.participants.length > 0) {
			update.participants = nextProps.participants;
			update.removeParticipants = false;
		}
		// length yapmazsam olmadı
		if (nextProps.participants !== undefined && nextProps.participants.length === 0) {
			update.removeParticipants = true;
		}
		return update;
	}
	componentDidMount() {
		
		this.props.loadParticipantsOfExpense(this.props.clickedExpense);
		
		
		
		
		// var joints = 0;
		// const { participants } = this.props.participant
		// for (let i = 0; i < participants.length; i++) {
		// 	if (participants.isJoint === 1) {
		// 		joints = joints + 1;
		// 	}
		// }
		
	}
	

	updateClickedMenuItem = () => {
		this.props.updateClickedMenuItem();
	};


	render() {

		if (this.props.clickedExpense !== undefined) {
			//console.log("CLICKED EXPENSE LENGTH : " + this.props.clickedExpense.length);
			console.log("CLICKED EXPENSE LENGTH : " + this.props.clickedExpense);
		} else {
			console.log("NoNE");
		}
		
		
		const {removeParticipants} = this.state;
		return (
			<Grid container>
				<Grid item xs={12} sm={12}>
					<Paper >
						<ExpenseCard clickedExpense={this.props.clickedExpense} />
					</Paper>
					<Paper style={{ padding: '4px', marginTop: '0px' }}>
						<h5> KATILIMCILAR..</h5>
					</Paper>
					
					{removeParticipants !== true && this.props.participants !== undefined ? this.props.participants.map((participant, index) => (<Paper style={{ padding: '20px' }} key={participant.participantid}>
								<ParticipantCard joint={this.props.joints[index]} amountOfJoints={this.props.amountOfJoints} participant={participant} />
							</Paper>)) : (<div></div>)}

					{this.state.openDialog === true ? (
						<DialogNewParticipant open={this.state.openDialog} close={this.handleCloseDialog} />
					) : null}
					<Fab onClick={this.openDialog} style={{ position: 'fixed', right: '12%', bottom: '8%' }} color="primary" aria-label="Add">
						<AddIcon />
					</Fab>
				</Grid>
			</Grid>
		);
	}
}
const mapDispatchToProps = {
	loadParticipantsOfExpense
};
const mapStateToProps = (state) => {
	
	return {
		loaded: state.expense.loaded,
		loading: state.expense.loading,
		error: state.expense.error,
		participants: state.participant.participants,
		clickedExpense: state.expense.clickedExpense,
		joints: state.participant.joints,
		amountOfJoints: state.participant.amountOfJoints
	};
};
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
