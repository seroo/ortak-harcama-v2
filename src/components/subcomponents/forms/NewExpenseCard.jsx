import React, { Component } from 'react';

import { connect } from 'react-redux';
import { TextField, Checkbox, Button, MenuItem, FormControlLabel } from '@material-ui/core';

// TODO Sag en ust koseye bi X koy kapatmak icin
class NewExpenseCard extends Component {
	state = {
		description: '',
		place: '',
		variety: 'yemek',
		isJoint: 1,
		amount: '',
		isChangeAThing: false
	};

	handleChange = (name) => (event) => {
		this.setState({
			...this.state,
			[name]: event.target.value,
			isChangeAThing: true
		});
	};
	handleCheckedChange = (name) => (event) => {
		this.setState({
			...this.state,
			[name]: event.target.checked === true ? 1 : 0,
			isChangeAThing: true
		});
	};

	// muhtemelen log u render a koymadığım için yukarıdaki metodla olmadı asagıdakini yaptım neyse
	handleSelect = (event) => {
		this.setState({
			...this.state,
			variety: event.target.value,
			isChangeAThing: true
		});
	};
	handleSubmit = (userId) => (e) => {
		e.preventDefault();
		const form = this.state;
		this.props.dialogFormSubmitted(form, userId);
		this.setState({ ...this.state, isChangeAThing: false });
	};
	handleClearForm = (e) => {
		e.preventDefault();
		this.setState({
			...this.state,
			description: '',
			place: '',
			variety: 'yemek',
			isJoint: 1,
			amount: '',
			isChangeAThing: false
		});
	};

	render() {
		if (this.state.amount === "") {
			this.state.isChangeAThing = false;
		}
		return (
			<div>
				<form onSubmit={this.handleSubmit}>
					<TextField
						style={{ margin: '10px 10px' }}
						label="Harcama adi"
						placeholder="Yemek, içmek vb.."
						onChange={this.handleChange('description')}
						value={this.state.description}
					/>
					<TextField
						style={{ margin: '10px 10px' }}
						onChange={this.handleChange('place')}
						value={this.state.place}
						label="Yer/Mekan"
						placeholder="Nusret/Chia vb.."
					/>
					<TextField
						style={{ margin: '10px 10px' }}
						label="Variety"
						select
						onChange={this.handleSelect}
						value={this.state.variety}
					>
						<MenuItem name={'yemek'} value={'yemek'}>
							Yemek
						</MenuItem>
						<MenuItem name={'icmek'} value={'icmek'}>
							Icmek
						</MenuItem>
						<MenuItem name={'etkinlik'} value={'etkinlik'}>
							Etkinlik
						</MenuItem>
						<MenuItem name={'diger'} value={'diger'}>
							Diger
						</MenuItem>
					</TextField>
					{/*<FormControlLabel*/}
						{/*control={*/}
							{/*<Checkbox*/}
								{/*checked={this.state.isJoint === 1}*/}
								{/*onChange={this.handleCheckedChange('isJoint')}*/}
								{/*color="primary"*/}
							{/*/>*/}
						{/*}*/}
						{/*label="Herkes ortak mı?"*/}
					{/*/>*/}
					<TextField
						style={{ margin: '10px 10px' }}
						label="Harcama Tutarı"
						type="number"
						value={this.state.amount}
						onChange={this.handleChange('amount')}
						placeholder="0.00"
					/>
				</form>
				<Button
					disabled={this.state.isChangeAThing === false}
					onClick={this.handleSubmit(this.props.userId)}
					variant="contained"
					color="primary"
				>
					Submit
				</Button>
				<Button
					style={{ marginLeft: '8px' }}
					onClick={this.handleClearForm}
					variant="contained"
					color="primary"
				>
					Clear
				</Button>
			</div>
		);
	}
}
const mapStateToProps = (state) => {
	return {
    userId: state.auth.userId,
	};
};

export default connect(mapStateToProps)(NewExpenseCard);
