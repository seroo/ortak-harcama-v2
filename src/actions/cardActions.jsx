import {
	LOAD_EXPENSES_FROM_SERVER,
	LOAD_PARTICIPANTS_FROM_SERVER,
	LOAD_EXPENSES_START,
	LOAD_EXPENSES_DONE,
	LOAD_EXPENSES_ERROR,
	LOAD_PARTICIPANTS_START,
	LOAD_PARTICIPANTS_DONE,
	LOAD_PARTICIPANTS_ERROR,
	SAVE_EXPENSE_START,
	SAVE_EXPENSE_DONE,
	SAVE_EXPENSE_ERROR,
	DELETE_EXPENSE_DONE,
	DELETE_EXPENSE_ERROR,
	DELETE_EXPENSE_START,
	UPDATE_EXPENSE_DONE,
	UPDATE_EXPENSE_ERROR,
	UPDATE_EXPENSE_START,
	CHANGE_CLICKED_EXPENSE_DONE,
	CHANGE_CLICKED_EXPENSE_ERROR,
    CHANGE_CLICKED_EXPENSE_START,
    ADD_PARTICIPANT_DONE,
    ADD_PARTICIPANT_ERROR,
    ADD_PARTICIPANT_START,
    DELETE_PARTICIPANT_DONE,
    DELETE_PARTICIPANT_ERROR,
    DELETE_PARTICIPANT_START,
    UPDATE_PARTICIPANT_DONE,
    UPDATE_PARTICIPANT_ERROR,
	UPDATE_PARTICIPANT_START,
	SIGN_IN,
	SIGN_OUT,
	TOGGLE_LEFT_PANEL,
	URL,
	localURL,
} from './actionConstants';
import axios from 'axios';

export const loadExpenseFromServer = (userId, expense = null) => {
	console.log(userId)
	if (userId === null) {
		return dispatch => ({ type: LOAD_EXPENSES_ERROR, payload: "invalid user" })
	}
	return async (dispatch) => {
		console.log("LOAD EXPENSE")
		dispatch({ type: LOAD_EXPENSES_START });
		await axios
			.get(`${URL}/api/expenses/${userId}`)
			.then(({ data }) => {
				console.log(data)
				dispatch({ type: LOAD_EXPENSES_FROM_SERVER });
				if (data.length > 0) {
					dispatch({ type: LOAD_EXPENSES_DONE, payload: data });
					if (expense === null) {
						dispatch(changeClickedExpense(data[0]));
						dispatch(loadParticipantsOfExpense(data[0]));
					}
					if (expense !== null) {
					dispatch(changeClickedExpense(expense));
					dispatch(loadParticipantsOfExpense(expense));
					}
				} else {
					dispatch({ type: LOAD_EXPENSES_DONE, payload: [] });
				}
			})
			.catch((error) => {
				dispatch({ type: LOAD_EXPENSES_ERROR, payload: error });
			});
	};
};

export const addNewExpense = (newExpense, userId) => {

	const { description, place, variety, isJoint, amount } = newExpense;
	console.log(JSON.stringify(newExpense));
	return (dispatch) => {
		dispatch({ type: SAVE_EXPENSE_START });
		axios
			.post(
				`${URL}/api/expenses/${userId}/add`, newExpense // sakin json a çevirme
			)
			.then(({ data }) => {
				// console.log(data)
				if (data) {
					dispatch({ type: SAVE_EXPENSE_DONE, payload: data });
					dispatch(loadExpenseFromServer(userId));
				}
			})
			.catch((error) => {
				dispatch({ type: SAVE_EXPENSE_ERROR, payload: error });
			});
	};
};
export const updateExpense = (updatedExpense, userId, clickedExpense) => {
	// clicked expense değismiyor burdan ve reducerdan değiştirerek belki oluır
	// sanki bi düzelme oldu kontrol et


	const { description, place, variety, isJoint, amount, expense } = updatedExpense;

	const { expenseid } = updatedExpense.expense;
	clickedExpense.amount = updatedExpense.amount
	clickedExpense.description = updatedExpense.description
	clickedExpense.place = updatedExpense.place
	clickedExpense.variety = updatedExpense.variety
	clickedExpense.isJoint = updatedExpense.isJoint

	return (dispatch) => {
		dispatch({ type: UPDATE_EXPENSE_START });
		axios
			.put(
				`${URL}/api/expenses/${userId}/update/${expenseid}`, updatedExpense
			)
			.then(({ data }) => {

				console.log(data)
				if (data === "{Expense updated}") {
					dispatch({ type: UPDATE_EXPENSE_DONE, payload: {data: data, updatedExpense: updatedExpense, clickedExpense: clickedExpense} })
					.then(dispatch(loadExpenseFromServer(userId, expense)))
					.then(dispatch(loadParticipantsOfExpense(updatedExpense)))
					.then(dispatch(changeClickedExpense(clickedExpense)));

					updatedExpense.expenseid = expenseid;



				} else {throw new PromiseRejectionEvent("Update expense failed")}
			})
			.catch((error) => {
				dispatch({ type: UPDATE_EXPENSE_ERROR, payload: error });
			});
	};
};

export const changeClickedExpense = (clickedExpense) => {
	return (dispatch) => {
		if (clickedExpense !== null) {
			dispatch({ type: CHANGE_CLICKED_EXPENSE_START });
			dispatch({ type: CHANGE_CLICKED_EXPENSE_DONE, payload: clickedExpense });
			dispatch(loadParticipantsOfExpense(clickedExpense));
		} else {
			dispatch({ type: CHANGE_CLICKED_EXPENSE_ERROR });
		}
	};
};
export const deleteExpense = (expenseid, userId) => {
	return (dispatch) => {
		dispatch({ type: DELETE_EXPENSE_START });
		axios
			.delete(`${URL}/api/expenses/${userId}/delete/${expenseid}`)
			.then(({ data }) => {
				if (data === "Expense Deleted") {
					dispatch({ type: DELETE_EXPENSE_DONE });
                    dispatch(loadExpenseFromServer(userId));

				} else {throw new PromiseRejectionEvent("Delete expense failed")}
			})
			.catch((error) => {
				dispatch({ type: DELETE_EXPENSE_ERROR, payload: error });
			});
	};
};

export const loadParticipantsOfExpense = (clickedExpense) => {

	const { expenseid, amount } = clickedExpense

	return async (dispatch) => {
		dispatch({ type: LOAD_PARTICIPANTS_START });
		await axios
			.get(`${URL}/api/participants/${expenseid}`)
			.then(({ data }) => {
				console.log(data)
				dispatch({ type: LOAD_PARTICIPANTS_FROM_SERVER });
				const { amount } = clickedExpense;
				var amountOfJoints = null
				if (data.length > 0) {
					var joints = data.map((row) => {
						return parseInt(row.isJoint)
				})

				amountOfJoints = amount / ((joints.filter((val) => {return val === 1}).length) + 1);
				}





				if (data.length > 0) {
					dispatch({ type: LOAD_PARTICIPANTS_DONE, payload: {data: data, joints: joints, amountOfJoints: amountOfJoints} });
				} else {
					dispatch({ type: LOAD_PARTICIPANTS_DONE, payload: data });
				}
			})
			.catch((error) => {
				dispatch({ type: LOAD_PARTICIPANTS_ERROR, payload: error });
			});
	};
};


export const addParticipantToClickedExpense = (newParticipant, clickedExpense) => {
    const { expenseid, name, description, isJoint, amountOfJoint, amountOfCut, isPaid } = newParticipant;

    return async (dispatch) => {
		dispatch({ type: ADD_PARTICIPANT_START });
		await axios
			.post(`${URL}/api/participants/${expenseid}/add`, newParticipant)
			.then(({ data }) => {
				console.log(data);
				if (data === "{Participant added}") {
					    dispatch({ type: ADD_PARTICIPANT_DONE, payload: data });
					    dispatch(loadParticipantsOfExpense(clickedExpense));
				} else {throw new PromiseRejectionEvent("Participant adding failed")}
			})
			.catch((error) => {
				dispatch({ type: ADD_PARTICIPANT_ERROR, payload: error });
			});
	};
}
export const deleteParticipantFromClickedExpense = (participantid, clickedExpense) => {
    return async (dispatch) => {
		dispatch({ type: DELETE_PARTICIPANT_START });
		await axios
			.delete(`${URL}/api/participants/delete/${participantid}`)
			.then(({ data }) => {
                dispatch({ type: DELETE_PARTICIPANT_DONE, payload: data });
                dispatch(loadParticipantsOfExpense(clickedExpense));
			})
			.catch((error) => {
				dispatch({ type: DELETE_PARTICIPANT_ERROR, payload: error });
			});
	};
}

export const updateParticipantCard = (updatedParticipantCard, clickedExpense) => {
	const { expenseid, participantid, name, description, isPaid, isJoint, amountOfJoint, amountOfCut } = updatedParticipantCard;

    return async (dispatch) => {
		dispatch({ type: UPDATE_PARTICIPANT_START });
		await axios
			.put(
				`${URL}/api/participants/update/${participantid}`, updatedParticipantCard
			)
			.then(({ data }) => {
				console.log(data)
					dispatch({ type: UPDATE_PARTICIPANT_DONE, payload: data });
					dispatch(loadParticipantsOfExpense(clickedExpense))
			})
			.catch((error) => {
				dispatch({ type: UPDATE_PARTICIPANT_ERROR, payload: error });
			});
	};
};

export const signIn = (userId) => {
	return {
		type: SIGN_IN,
		payload: userId
	}
}
export const signOut = () => {
	return {
		type: SIGN_OUT
	}
}

export const toggleLeftPanel = () =>{
	return { type: TOGGLE_LEFT_PANEL }
}