import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import NewParticipantCard from './NewParticipantCard';
import { connect } from 'react-redux';

import { addParticipantToClickedExpense } from '../../../actions/cardActions';

class DialogNewParticipant extends Component {
    state = {
		open: false,

    };
    
    handleClickOpen = () => {
		this.setState({ open: true });
	};

	handleClose = () => {
        this.props.close();
		this.setState({...this.state, open: false });
	};
	componentDidMount() {
		if (this.props.open) {
			this.setState({ ...this.state, open: this.props.open });
		}
	}

	dialogFormSubmitted = (newParticipant) => {
		//formu bir uste gonder
		this.props.addParticipantToClickedExpense(newParticipant, this.props.clickedExpense);
		this.props.close();
		
	}

    render() {
        return (
            <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
				<DialogTitle id="form-dialog-title">New Participant</DialogTitle>
				<DialogContent>
                    <NewParticipantCard clickedExpense={this.props.clickedExpense} onClose={this.handleClose} dialogFormSubmitted={this.dialogFormSubmitted} />
				</DialogContent>
				
			</Dialog>
        );
    }
}

const mapStateToProps = (state) => {
	return {
		clickedExpense: state.expense.clickedExpense,
	}
}

const mapDispatchToProps = {
	addParticipantToClickedExpense,
}


export default connect(mapStateToProps, mapDispatchToProps)(DialogNewParticipant);