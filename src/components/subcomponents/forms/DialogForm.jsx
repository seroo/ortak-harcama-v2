import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import NewExpenseCard from './NewExpenseCard'

class DialogForm extends React.Component {
	state = {
		open: false,

	};

	handleClickOpen = () => {
		this.setState({ open: true });
	};

	handleClose = () => {
        this.props.close();
		this.setState({...this.state, open: false });
	};
	componentDidMount() {
		if (this.props.open) {
			this.setState({ ...this.state, open: this.props.open });
		}
	}

	dialogFormSubmitted = (form, userId) => {
		//formu bir uste gonder
		this.props.handleAddNewExpense(form, userId);
		this.props.close();
		
	}

	render() {
		return (
			<Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
				<DialogTitle id="form-dialog-title">New Expense</DialogTitle>
				<DialogContent>
                    <NewExpenseCard onClose={this.handleClose} dialogFormSubmitted={this.dialogFormSubmitted} />
				</DialogContent>
				
			</Dialog>
		);
	}
}

export default DialogForm;
