import {
	LOAD_PARTICIPANTS_FROM_SERVER,
	LOAD_PARTICIPANTS_DONE,
	LOAD_PARTICIPANTS_START,
	LOAD_PARTICIPANTS_ERROR,
	ADD_PARTICIPANT_DONE,
    ADD_PARTICIPANT_ERROR,
    ADD_PARTICIPANT_START,
    DELETE_PARTICIPANT_DONE,
    DELETE_PARTICIPANT_ERROR,
    DELETE_PARTICIPANT_START,
    UPDATE_PARTICIPANT_DONE,
    UPDATE_PARTICIPANT_ERROR,
    UPDATE_PARTICIPANT_START,
} from '../actions/actionConstants';

const initialState = {
	loaded: false,
	loading: false,
	error: null,
	participants: [],
	saving: false,
	deleting: false,
	updating: false
};

const participantsReducer = (state = initialState, action) => {
	switch (action.type) {
		case ADD_PARTICIPANT_START:
			return {...state, loading: true};
		case ADD_PARTICIPANT_DONE:
			return {...state, loading: false, loaded:true};
		case ADD_PARTICIPANT_ERROR:
			return {...state, loading: false, error: action.payload};
		case UPDATE_PARTICIPANT_START:
			return {...state, loading: true};
		case UPDATE_PARTICIPANT_DONE:
			return {...state, loading: false, loaded:true};
		case UPDATE_PARTICIPANT_ERROR:
			return {...state, loading: false, error: action.payload};
		case DELETE_PARTICIPANT_START:
			return {...state, deleting: true};
		case DELETE_PARTICIPANT_DONE:
			return {...state, deleting: false};
		case DELETE_PARTICIPANT_ERROR:
			return {...state, loading: false, error: action.payload};
		case LOAD_PARTICIPANTS_START:
			return { ...state, loading: true };
		case LOAD_PARTICIPANTS_FROM_SERVER:
			return { ...state, loading: true };
		case LOAD_PARTICIPANTS_ERROR:
			return { ...state, loading: false, error: action.payload };
		case LOAD_PARTICIPANTS_DONE:
			return {
				...state,
				loaded: true,
				loading: false,
				error: null,
				participants: action.payload.data,
				joints: action.payload.joints,
				amountOfJoints: action.payload.amountOfJoints,
			};
		default:
			return state;
	}
};

export default participantsReducer;
