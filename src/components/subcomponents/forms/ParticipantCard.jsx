import React, { Component } from 'react';
import { TextField, FormControlLabel, Checkbox, Button } from '@material-ui/core';
import DeleteAlertDialog from '../DeleteAlertDialog';
import { deleteParticipantFromClickedExpense, updateParticipantCard, loadParticipantsOfExpense } from '../../../actions/cardActions'
import { connect } from 'react-redux';


class ParticipantCard extends Component {
	state = {
		participantid: -1,
		name: '',
		description: '',
		isJoint: 0,
		isPaid: 0,
		amountOfJoint: 0.0,
		amountOfCut: parseFloat(0.0),
		expenseid: -1,
		isChangeAThing: false
	};

	static getDerivedStateFromProps(nextProps, prevState) {
		let update = {};

		// if (nextProps.showMenu !== []) {
		// 	update.showMenu = nextProps.showMenu;
		// }
		if (
			!(Object.keys(nextProps.participant).length === 0 && nextProps.participant.constructor === Object) &&
			prevState.participantid !== nextProps.participant.participantid
		) {
			update.participantid = nextProps.participant.participantid;
			update.expenseid = nextProps.participant.expenseid;
			update.name = nextProps.participant.name;
			update.description = nextProps.participant.description;
			update.isJoint = parseInt(nextProps.participant.isJoint);
			update.isPaid = parseInt(nextProps.participant.isPaid);
			update.amountOfJoint = parseFloat(nextProps.participant.amountOfJoint);
			update.amountOfCut = parseFloat(nextProps.participant.amountOfCut);
		}
		
		if (nextProps.clickedExpense.amountOfJoint !== prevState.amountOfJoint) {
			update.amountOfJoint = parseFloat(nextProps.participant.amountOfJoint);
		}

		return update;
	}

	// componentDidMount() {
	// 	this.setState({
	// 		...this.state,
	// 		participantid: this.props.participant.participantid,
	// 		name: this.props.participant.name,
	// 		description: this.props.participant.description,
	// 		isJoint: parseInt(this.props.participant.isJoint),
	// 		isPaid: parseInt(this.props.participant.isPaid),
	// 		amountOfJoint: this.props.participant.amountOfJoint,
	// 		amountOfCut: this.props.participant.amountOfCut
	// 	});
	// }

	openDialog = () => {
		this.setState({
			...this.state,
			openDialog: !this.state.openDialog
		});
	};
	handleCloseDialog = () => {
		this.setState({
			...this.state,
			openDialog: false
		});
	};

	handleChange = (name) => (event) => {
		this.setState({
			...this.state,
			[name]: event.target.value,
			isChangeAThing: true,
		});
	};

	handleCheckedChange = (name) => (event) => {
		this.setState({
			...this.state,
			[name]: event.target.checked ? 1 : 0,
			isChangeAThing: true,
		});
	};

	handleDelete = () => {
		this.props.deleteParticipantFromClickedExpense(this.state.participantid, this.props.clickedExpense)

	}

	handleUpdate = () => {
			this.props.updateParticipantCard(this.state, this.props.clickedExpense);
			this.setState({
				...this.state, isChangeAThing: false,
			})
	}
	render() {
		if (this.state.name === "") {
			this.state.isChangeAThing = false;
		}
		const {joint, amountOfJoints} = this.props;

		return (
			<div>
				<form>
					<TextField
						style={{ margin: '10px 10px' }}
						label="Name"
						placeholder="Katilimci Adi"
						onChange={this.handleChange('name')}
						value={this.state.name}
					/>
					<TextField
						style={{ margin: '10px 10px' }}
						label="Aciklama"
						multiline
						onChange={this.handleChange('description')}
						value={this.state.description}
						placeholder="Aciklama.."
					/>
					<FormControlLabel
						control={
							<Checkbox
								checked={this.state.isJoint === 1}
								onChange={this.handleCheckedChange('isJoint')}
								color="primary"
							/>
						}
						label="Harcamaya katılıyor.."
					/>
					<FormControlLabel
						control={
							<Checkbox
								checked={this.state.isPaid === 1}
								onChange={this.handleCheckedChange('isPaid')}
								color="primary"
							/>
						}
						label="Ödeme alındı mı.."
					/>
					<TextField
						style={{ margin: '10px 10px' }}
						label="Uygulanacak indirim"
						type="number"
						
						value={parseFloat(this.state.amountOfCut)}
						onChange={this.handleChange('amountOfCut')}
						placeholder="0.00"
					/>
					<TextField
						style={{ margin: '10px 10px' }}
						id="outlined-disabled"
						variant="outlined"
						label="Harcamadaki Payı"
						type="number"
						disabled
						value={joint === 0 ? 0.00 : ((joint === 1 ? amountOfJoints : 0) - this.state.amountOfCut).toFixed(2)}
						placeholder="0.00"
					/>
				</form>
				<div style={{ marginTop: '32px', marginLeft: '10px' }}>
					<Button
						disabled={this.state.isChangeAThing === false}
						variant="contained"
						color="primary"
						onClick={this.handleUpdate}
					>
						Submit
					</Button>
					<Button
						style={{ marginLeft: '16px' }}
						onClick={this.openDialog}
						variant="contained"
						color="secondary"
					>
						Delete
					</Button>
					{this.state.openDialog === true ? (
						<DeleteAlertDialog
							open={this.state.openDialog}
							close={this.handleCloseDialog}
							handleDelete={this.handleDelete}
						/>
					) : null}
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {

	return {
		clickedExpense: state.expense.clickedExpense,
	}
}

const mapDispatchToProps = {
	deleteParticipantFromClickedExpense,
	updateParticipantCard,
	loadParticipantsOfExpense,
}

export default connect(mapStateToProps, mapDispatchToProps) (ParticipantCard);
